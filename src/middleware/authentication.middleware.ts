import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";

import HttpError from "../exceptions/HttpError.exception";
import Token from "../interfaces/token.interface";
import { verifyToken } from "../utils/token";
import UserModel from "../api/v1/user/User.model";

export const Authenticated = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (req.method === "OPTIONS") {
    return next();
  }

  try {
    const accessToken = req?.headers?.authorization;
    if (!accessToken || !accessToken.startsWith("Bearer ")) {
      console.log("Unauthorized");
      return next(new HttpError("Unauthorized", 401));
    }
    const token = accessToken.split("Bearer ")[1].trim(); // Authorization: 'Bearer TOKEN'
    if (!token) {
      console.log("Unauthorized!");
      return next(new HttpError("Unauthorized!", 401));
    }
    const decodedToken: Token | jwt.JsonWebTokenError = await verifyToken(
      token
    );
    // new Promise(
    //   (resolve, reject) => {
    //     jwt.verify(token, process.env.JWT_KEY as jwt.Secret, (err, payload) => {
    //       if (err) return reject(err);
    //       resolve(payload as Token);
    //     });
    //   }
    // );

    if (decodedToken instanceof jwt.JsonWebTokenError) {
      console.log("Authentication failed with errors!");
      console.log(decodedToken);
      return next(new HttpError("Authentication failed with errors!", 401));
    }

    const user = await UserModel.findById(decodedToken.userId)
      .select("-password")
      .exec();

    if (!user) {
      console.log("Not found!");
      return next(new HttpError("Not found!", 401));
    }

    req.user = user;

    next();
  } catch (err) {
    console.log(err);
    const error = new HttpError("Authentication failed!", 401);
    return next(error);
  }
};
