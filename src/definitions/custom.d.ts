import IUserModel from '../api/v1/user/user.interface';

declare global {
    namespace Express {
        export interface Request {
            user: IUserModel;
        }
    }
}
