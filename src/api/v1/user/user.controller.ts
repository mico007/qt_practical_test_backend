import { Request, Response, NextFunction } from "express";
import { validationResult } from "express-validator";
import HttpError from "../../../exceptions/HttpError.exception";
import { registerUser, login } from "./user.service";
import IUserModel from "./user.interface";
import UserModel from "./User.model";
import bcrypt from "bcrypt";

export const createUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log(errors);
    return next(
      new HttpError("Invalid inputs passed, please check your data.", 400)
    );
  }

  const {
    email,
    password,
    confirmPswd,
    fullName,
    age,
    phone,
    sex,
    personalId,
    address,
  } = req.body;

  try {
    const user = await registerUser(
      email,
      password,
      confirmPswd,
      fullName,
      age,
      phone,
      sex,
      personalId,
      address,
      next
    );

    user ? res.status(201).json({ user: user }) : next(user);
  } catch (err) {
    console.log(err);
    return next(new HttpError("Creating user failed", 500));
  }
};

export const loginUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log(errors);
    return next(
      new HttpError("Invalid inputs passed, please check your data.", 400)
    );
  }

  const { email, password } = req.body;

  try {
    const user = await login(email, password, next);
    user ? res.status(200).json({ user: user }) : next(user);
  } catch (err) {
    console.log(err);
    return next(new HttpError("Login failed", 500));
  }
};

export const getUserProfile = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let userProfile: IUserModel | null;
  try {
    userProfile = await UserModel.findOne({ email: req.user.email })
      .select("-password")
      .exec();
  } catch (err) {
    console.log(err);
    return next(new HttpError("Fetching profile failed", 500));
  }

  if (!userProfile) {
    return next(new HttpError("Profile not found", 404));
  }

  res.status(200).json({ user: userProfile });
};

export const updateUserProfile = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log(errors);
    return next(
      new HttpError("Invalid inputs passed, please check your data.", 400)
    );
  }

  const { fullName, age, phone, sex, personalId, address } = req.body;

  let existingUser: IUserModel | null;

  try {
    existingUser = await UserModel.findOne({ email: req.user.email });
  } catch (err) {
    console.log(err);
    return next(new HttpError("Fetching user failed", 500));
  }
  if (!existingUser) {
    return next(new HttpError("User not found", 404));
  }

  existingUser.fullName = fullName;
  existingUser.age = age;
  existingUser.phone = phone;
  existingUser.address = address;
  existingUser.sex = sex;
  existingUser.personalId = personalId;

  let updatedUser: IUserModel;

  try {
    updatedUser = await existingUser.save();
  } catch (err) {
    console.log(err);
    throw new HttpError("Updating user failed", 500);
  }

  res.status(200).json({ user: updatedUser });
};

export const resetPassword = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log(errors);
    return next(
      new HttpError("Invalid inputs passed, please check your data.", 400)
    );
  }
  const { newPassword, confirmPswd } = req.body;

  if (newPassword !== confirmPswd) {
    console.log("Passwords do not match");
    return next(new HttpError("Passwords do not match", 422));
  }

  let existingUser: IUserModel | null;
  try {
    existingUser = await UserModel.findOne({ email: req.user.email });
  } catch (err) {
    console.log(err);
    return next(new HttpError("Fetching user failed", 500));
  }
  if (!existingUser) {
    return next(new HttpError("User not found", 404));
  }

  let hashedPassword: string;
  try {
    hashedPassword = await bcrypt.hash(newPassword, 12);
  } catch (err) {
    console.log(err);
    return next(new HttpError("Could not reset password", 500));
  }

  existingUser.password = hashedPassword;

  try {
    await existingUser.save();
  } catch (err) {
    console.log(err);
    throw new HttpError("Resetting password failed", 500);
  }

  res.status(200).json({message: "Password reset successfully"});
};
