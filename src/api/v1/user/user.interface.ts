import { Document } from "mongoose";

export default interface IUserModel extends Document {
    email: string;
    password: string;
    fullName: string;
    phone: string;
    address: string;
    age: number;
    personalId: string;
    sex: string;
}