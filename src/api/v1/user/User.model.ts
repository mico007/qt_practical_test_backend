import { Schema, model } from "mongoose";
import IUserModel from './user.interface';

const UserSchema = new Schema(
  {
    email: {
      type: String,
      require: true,
    },
    password: {
      type: String,
      require: true,
    },
    fullName: {
      type: String,
      require: true,
    },
    phone: {
        type: String,
        require: true,
    },
    address: {
        type: String,
        require: true,
    },
    sex: {
        type: String,
        require: true,
    },
    age: {
        type: Number,
        require: true,
    },
    personalId: {
        type: String,
        require: true,
    }
  },
  { timestamps: true }
);

export default model<IUserModel>("users", UserSchema);
