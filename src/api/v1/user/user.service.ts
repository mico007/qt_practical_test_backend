import { NextFunction } from "express";
import UserModel from "./User.model";
import HttpError from "../../../exceptions/HttpError.exception";
import IUserModel from "./user.interface";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

export const registerUser = async (
  email: string,
  password: string,
  confirmPswd: string,
  fullName: string,
  age: number,
  sex: string,
  address: string,
  phone: string,
personalId: string,
  next: NextFunction
): Promise<IUserModel | void> => {
  if (password !== confirmPswd) {
    console.log("Passwords do not match");
    return next(new HttpError("Passwords do not match", 422));
  }

  let existingUser: IUserModel | null;
  try {
    existingUser = await UserModel.findOne({ email: email });
  } catch (err) {
    console.log(err);
    return next(new HttpError("Fetching user failed", 500));
  }
  if (existingUser) {
    return next(
      new HttpError("User exists already, please login instead", 406)
    );
  }

  let hashedPassword: string;
  try {
    hashedPassword = await bcrypt.hash(password, 12);
  } catch (err) {
    console.log(err);
    return next(new HttpError("Could not create user", 500));
  }

  const user = new UserModel({
    email,
    password: hashedPassword,
    confirmPswd,
    fullName,
    age,
    sex,
    address,
    phone,
    personalId,
  });

  try {
    const savedUser = await user.save();
    return savedUser;
  } catch (err) {
    console.log(err);
    throw new HttpError("Registering user failed", 500);
  }
};

export const login = async (
  email: string,
  password: string,
  next: NextFunction
) => {
  let existingUser: IUserModel | null;
  try {
    existingUser = await UserModel.findOne({ email: email });
  } catch (err) {
    console.log(err);
    return next(new HttpError("Fetching user failed", 500));
  }
  if (!existingUser) {
    return next(
      new HttpError("Invalid credentials, could not log you in.", 422)
    );
  }

  let isValidPassword = false;
  try {
    isValidPassword = await bcrypt.compare(password, existingUser.password);
  } catch (err) {
    console.log(err);
    return next(new HttpError("Could not log you in, try again.", 500));
  }
  if (!isValidPassword) {
    return next(
      new HttpError("Invalid credentials, could not log you in.", 422)
    );
  }

  let token: string;
  try {
    token = jwt.sign(
      { userId: existingUser._id },
      process.env.JWT_KEY as jwt.Secret,
      { expiresIn: "1h" }
    );
  } catch (err) {
    console.log(err);
    return next(new HttpError("Logging in failed, please try again.", 500));
  }

  return {
    userId: existingUser._id as string,
    token: token,
    email: existingUser.email,
  };
};
