import { Router } from "express";
const router = Router();
import { check } from "express-validator";

import {
  createUser,
  loginUser,
  getUserProfile,
  updateUserProfile,
  resetPassword,
} from "../api/v1/user/user.controller";
import { Authenticated } from "../middleware/authentication.middleware";

router.post(
  "/register",
  [
    check("email").notEmpty().isEmail(),
    check("password").notEmpty().isLength({ min: 6 }),
    check("fullName").notEmpty(),
    check("phone").notEmpty(),
    check("address").notEmpty(),
    check("sex").notEmpty(),
    check("age").notEmpty(),
    check("personalId").notEmpty(),
    check("confirmPswd").notEmpty().isLength({ min: 6 }),
  ],
  createUser
);

router.post(
  "/login",
  [
    check("email").notEmpty().isEmail(),
    check("password").notEmpty().isLength({ min: 6 }),
  ],
  loginUser
);

router.get("/profile", Authenticated, getUserProfile);

router.put(
  "/profile",
  Authenticated,
  [
    check("phone").notEmpty(),
    check("address").notEmpty(),
    check("sex").notEmpty(),
    check("age").notEmpty(),
    check("personalId").notEmpty(),
    check("fullName").notEmpty(),
  ],
  updateUserProfile
);

router.put(
  "/reset-password",
  Authenticated,
  [
    check("newPassword").notEmpty().isLength({ min: 6 }),
    check("confirmPswd").notEmpty().isLength({ min: 6 }),
  ],
  resetPassword
);

export default router;
