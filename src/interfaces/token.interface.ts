import { Schema } from 'mongoose';

interface Token extends Object {
    userId: Schema.Types.ObjectId;
    expiresIn: number;
}

export default Token;
