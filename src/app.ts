import express, { Request, Response, NextFunction } from "express";
import mongoose from "mongoose";

import HttpError  from "./exceptions/HttpError.exception";
import userRouter from "./routers/user.router";

mongoose
  .connect("mongodb://localhost:27017/qt_task_a_user_management")
  .then(() => {
    console.log("MongoDB Connected");
  })
  .catch((err) => {
    console.log("MongoDB Connection Error: " + err);
  });

const app = express();

app.use(express.json());

app.use((req: Request, res: Response, next: NextFunction) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, DELETE, OPTIONS"
  );
  next();
});

app.use("/api/v1/users", userRouter);

app.use((req: Request, res: Response, next: NextFunction) => {
  return next(new HttpError("Could not find this route", 404));
});

app.use((error: HttpError, req: Request, res: Response, next: NextFunction) => {
  if (res.headersSent) {
    return next(error);
  }
  res.status(error.code || 500);
  res.json({ message: error.message || "Something went wrong" });
});

const port = process.env.PORT ?? 8000;
app.listen(port, () => {
  console.log("Server is running on port 8000");
});

