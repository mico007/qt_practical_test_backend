"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const UserSchema = new mongoose_1.Schema({
    email: {
        type: String,
        require: true,
    },
    password: {
        type: String,
        require: true,
    },
    fullName: {
        type: String,
        require: true,
    },
    phone: {
        type: String,
        require: true,
    },
    address: {
        type: String,
        require: true,
    },
    sex: {
        type: String,
        require: true,
    },
    age: {
        type: Number,
        require: true,
    },
    personalId: {
        type: String,
        require: true,
    }
}, { timestamps: true });
exports.default = (0, mongoose_1.model)("users", UserSchema);
