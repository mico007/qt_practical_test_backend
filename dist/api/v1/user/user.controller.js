"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.resetPassword = exports.updateUserProfile = exports.getUserProfile = exports.loginUser = exports.createUser = void 0;
const express_validator_1 = require("express-validator");
const HttpError_exception_1 = __importDefault(require("../../../exceptions/HttpError.exception"));
const user_service_1 = require("./user.service");
const User_model_1 = __importDefault(require("./User.model"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const createUser = async (req, res, next) => {
    const errors = (0, express_validator_1.validationResult)(req);
    if (!errors.isEmpty()) {
        console.log(errors);
        return next(new HttpError_exception_1.default("Invalid inputs passed, please check your data.", 400));
    }
    const { email, password, confirmPswd, fullName, age, phone, sex, personalId, address, } = req.body;
    try {
        const user = await (0, user_service_1.registerUser)(email, password, confirmPswd, fullName, age, phone, sex, personalId, address, next);
        user ? res.status(201).json({ user: user }) : next(user);
    }
    catch (err) {
        console.log(err);
        return next(new HttpError_exception_1.default("Creating user failed", 500));
    }
};
exports.createUser = createUser;
const loginUser = async (req, res, next) => {
    const errors = (0, express_validator_1.validationResult)(req);
    if (!errors.isEmpty()) {
        console.log(errors);
        return next(new HttpError_exception_1.default("Invalid inputs passed, please check your data.", 400));
    }
    const { email, password } = req.body;
    try {
        const user = await (0, user_service_1.login)(email, password, next);
        user ? res.status(200).json({ user: user }) : next(user);
    }
    catch (err) {
        console.log(err);
        return next(new HttpError_exception_1.default("Login failed", 500));
    }
};
exports.loginUser = loginUser;
const getUserProfile = async (req, res, next) => {
    let userProfile;
    try {
        userProfile = await User_model_1.default.findOne({ email: req.user.email })
            .select("-password")
            .exec();
    }
    catch (err) {
        console.log(err);
        return next(new HttpError_exception_1.default("Fetching profile failed", 500));
    }
    if (!userProfile) {
        return next(new HttpError_exception_1.default("Profile not found", 404));
    }
    res.status(200).json({ user: userProfile });
};
exports.getUserProfile = getUserProfile;
const updateUserProfile = async (req, res, next) => {
    const errors = (0, express_validator_1.validationResult)(req);
    if (!errors.isEmpty()) {
        console.log(errors);
        return next(new HttpError_exception_1.default("Invalid inputs passed, please check your data.", 400));
    }
    const { fullName, age, phone, sex, personalId, address } = req.body;
    let existingUser;
    try {
        existingUser = await User_model_1.default.findOne({ email: req.user.email });
    }
    catch (err) {
        console.log(err);
        return next(new HttpError_exception_1.default("Fetching user failed", 500));
    }
    if (!existingUser) {
        return next(new HttpError_exception_1.default("User not found", 404));
    }
    existingUser.fullName = fullName;
    existingUser.age = age;
    existingUser.phone = phone;
    existingUser.address = address;
    existingUser.sex = sex;
    existingUser.personalId = personalId;
    let updatedUser;
    try {
        updatedUser = await existingUser.save();
    }
    catch (err) {
        console.log(err);
        throw new HttpError_exception_1.default("Updating user failed", 500);
    }
    res.status(200).json({ user: updatedUser });
};
exports.updateUserProfile = updateUserProfile;
const resetPassword = async (req, res, next) => {
    const errors = (0, express_validator_1.validationResult)(req);
    if (!errors.isEmpty()) {
        console.log(errors);
        return next(new HttpError_exception_1.default("Invalid inputs passed, please check your data.", 400));
    }
    const { newPassword, confirmPswd } = req.body;
    if (newPassword !== confirmPswd) {
        console.log("Passwords do not match");
        return next(new HttpError_exception_1.default("Passwords do not match", 422));
    }
    let existingUser;
    try {
        existingUser = await User_model_1.default.findOne({ email: req.user.email });
    }
    catch (err) {
        console.log(err);
        return next(new HttpError_exception_1.default("Fetching user failed", 500));
    }
    if (!existingUser) {
        return next(new HttpError_exception_1.default("User not found", 404));
    }
    let hashedPassword;
    try {
        hashedPassword = await bcrypt_1.default.hash(newPassword, 12);
    }
    catch (err) {
        console.log(err);
        return next(new HttpError_exception_1.default("Could not reset password", 500));
    }
    existingUser.password = hashedPassword;
    try {
        await existingUser.save();
    }
    catch (err) {
        console.log(err);
        throw new HttpError_exception_1.default("Resetting password failed", 500);
    }
    res.status(200).json({ message: "Password reset successfully" });
};
exports.resetPassword = resetPassword;
