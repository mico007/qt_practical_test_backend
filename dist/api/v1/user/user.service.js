"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.login = exports.registerUser = void 0;
const User_model_1 = __importDefault(require("./User.model"));
const HttpError_exception_1 = __importDefault(require("../../../exceptions/HttpError.exception"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const registerUser = async (email, password, confirmPswd, fullName, age, sex, address, phone, personalId, next) => {
    if (password !== confirmPswd) {
        console.log("Passwords do not match");
        return next(new HttpError_exception_1.default("Passwords do not match", 422));
    }
    let existingUser;
    try {
        existingUser = await User_model_1.default.findOne({ email: email });
    }
    catch (err) {
        console.log(err);
        return next(new HttpError_exception_1.default("Fetching user failed", 500));
    }
    if (existingUser) {
        return next(new HttpError_exception_1.default("User exists already, please login instead", 406));
    }
    let hashedPassword;
    try {
        hashedPassword = await bcrypt_1.default.hash(password, 12);
    }
    catch (err) {
        console.log(err);
        return next(new HttpError_exception_1.default("Could not create user", 500));
    }
    const user = new User_model_1.default({
        email,
        password: hashedPassword,
        confirmPswd,
        fullName,
        age,
        sex,
        address,
        phone,
        personalId,
    });
    try {
        const savedUser = await user.save();
        return savedUser;
    }
    catch (err) {
        console.log(err);
        throw new HttpError_exception_1.default("Registering user failed", 500);
    }
};
exports.registerUser = registerUser;
const login = async (email, password, next) => {
    let existingUser;
    try {
        existingUser = await User_model_1.default.findOne({ email: email });
    }
    catch (err) {
        console.log(err);
        return next(new HttpError_exception_1.default("Fetching user failed", 500));
    }
    if (!existingUser) {
        return next(new HttpError_exception_1.default("Invalid credentials, could not log you in.", 422));
    }
    let isValidPassword = false;
    try {
        isValidPassword = await bcrypt_1.default.compare(password, existingUser.password);
    }
    catch (err) {
        console.log(err);
        return next(new HttpError_exception_1.default("Could not log you in, try again.", 500));
    }
    if (!isValidPassword) {
        return next(new HttpError_exception_1.default("Invalid credentials, could not log you in.", 422));
    }
    let token;
    try {
        token = jsonwebtoken_1.default.sign({ userId: existingUser._id }, process.env.JWT_KEY, { expiresIn: "1h" });
    }
    catch (err) {
        console.log(err);
        return next(new HttpError_exception_1.default("Logging in failed, please try again.", 500));
    }
    return {
        userId: existingUser._id,
        token: token,
        email: existingUser.email,
    };
};
exports.login = login;
