"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = (0, express_1.Router)();
const express_validator_1 = require("express-validator");
const user_controller_1 = require("../api/v1/user/user.controller");
const authentication_middleware_1 = require("../middleware/authentication.middleware");
router.post("/register", [
    (0, express_validator_1.check)("email").notEmpty().isEmail(),
    (0, express_validator_1.check)("password").notEmpty().isLength({ min: 6 }),
    (0, express_validator_1.check)("fullName").notEmpty(),
    (0, express_validator_1.check)("phone").notEmpty(),
    (0, express_validator_1.check)("address").notEmpty(),
    (0, express_validator_1.check)("sex").notEmpty(),
    (0, express_validator_1.check)("age").notEmpty(),
    (0, express_validator_1.check)("personalId").notEmpty(),
    (0, express_validator_1.check)("confirmPswd").notEmpty().isLength({ min: 6 }),
], user_controller_1.createUser);
router.post("/login", [
    (0, express_validator_1.check)("email").notEmpty().isEmail(),
    (0, express_validator_1.check)("password").notEmpty().isLength({ min: 6 }),
], user_controller_1.loginUser);
router.get("/profile", authentication_middleware_1.Authenticated, user_controller_1.getUserProfile);
router.put("/profile", authentication_middleware_1.Authenticated, [
    (0, express_validator_1.check)("phone").notEmpty(),
    (0, express_validator_1.check)("address").notEmpty(),
    (0, express_validator_1.check)("sex").notEmpty(),
    (0, express_validator_1.check)("age").notEmpty(),
    (0, express_validator_1.check)("personalId").notEmpty(),
    (0, express_validator_1.check)("fullName").notEmpty(),
], user_controller_1.updateUserProfile);
router.put("/reset-password", authentication_middleware_1.Authenticated, [
    (0, express_validator_1.check)("newPassword").notEmpty().isLength({ min: 6 }),
    (0, express_validator_1.check)("confirmPswd").notEmpty().isLength({ min: 6 }),
], user_controller_1.resetPassword);
exports.default = router;
