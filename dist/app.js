"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const HttpError_exception_1 = __importDefault(require("./exceptions/HttpError.exception"));
const user_router_1 = __importDefault(require("./routers/user.router"));
mongoose_1.default
    .connect("mongodb://localhost:27017/qt_task_a_user_management")
    .then(() => {
    console.log("MongoDB Connected");
})
    .catch((err) => {
    console.log("MongoDB Connection Error: " + err);
});
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});
app.use("/api/v1/users", user_router_1.default);
app.use((req, res, next) => {
    return next(new HttpError_exception_1.default("Could not find this route", 404));
});
app.use((error, req, res, next) => {
    if (res.headersSent) {
        return next(error);
    }
    res.status(error.code || 500);
    res.json({ message: error.message || "Something went wrong" });
});
const port = (_a = process.env.PORT) !== null && _a !== void 0 ? _a : 8000;
app.listen(port, () => {
    console.log("Server is running on port 8000");
});
