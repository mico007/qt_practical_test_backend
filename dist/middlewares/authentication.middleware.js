"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Authenticated = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const HttpError_exception_1 = __importDefault(require("../exceptions/HttpError.exception"));
const token_1 = require("../utils/token");
const User_model_1 = __importDefault(require("../api/v1/user/User.model"));
const Authenticated = async (req, res, next) => {
    var _a;
    if (req.method === "OPTIONS") {
        return next();
    }
    try {
        const accessToken = (_a = req === null || req === void 0 ? void 0 : req.headers) === null || _a === void 0 ? void 0 : _a.authorization;
        if (!accessToken || !accessToken.startsWith("Bearer ")) {
            console.log("Unauthorized");
            return next(new HttpError_exception_1.default("Unauthorized", 401));
        }
        const token = accessToken.split("Bearer ")[1].trim(); // Authorization: 'Bearer TOKEN'
        if (!token) {
            console.log("Unauthorized!");
            return next(new HttpError_exception_1.default("Unauthorized!", 401));
        }
        const decodedToken = await (0, token_1.verifyToken)(token);
        // new Promise(
        //   (resolve, reject) => {
        //     jwt.verify(token, process.env.JWT_KEY as jwt.Secret, (err, payload) => {
        //       if (err) return reject(err);
        //       resolve(payload as Token);
        //     });
        //   }
        // );
        if (decodedToken instanceof jsonwebtoken_1.default.JsonWebTokenError) {
            console.log("Authentication failed with errors!");
            console.log(decodedToken);
            return next(new HttpError_exception_1.default("Authentication failed with errors!", 401));
        }
        const user = await User_model_1.default.findById(decodedToken.userId)
            .select("-password")
            .exec();
        if (!user) {
            console.log("Not found!");
            return next(new HttpError_exception_1.default("Not found!", 401));
        }
        req.user = user;
        next();
    }
    catch (err) {
        console.log(err);
        const error = new HttpError_exception_1.default("Authentication failed!", 401);
        return next(error);
    }
};
exports.Authenticated = Authenticated;
